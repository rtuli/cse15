#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>
#include"Dictionary.h"


int main(){
    //create a new Dictionary
    Dictionary D1 = newDictionary();

    //insert some pairs into the dictionary
    insert(D1, "J", "Church");    
    insert(D1, "K", "Ingleside");
    insert(D1, "L", "Taraval");
    insert(D1, "M", "OceanView");
    insert(D1, "N", "Judah");
    insert(D1, "T", "Third"); 
   
    //print out the dictionary
    char *stringD1 = DictionaryToString(D1);
    printf("%s", stringD1);
    free(stringD1);

    //make sure the list is the expected length
    if (size(D1) != 6) {
    	printf("Error: Dictionary not expected size.\n");
    } 



    if (strcmp(lookup(D1, "T"), "Third") != 0) {
    	printf("Error: Lookup does not return expected value for key T.\n");
    }
    if (strcmp(lookup(D1, "N"), "Judah") != 0) {
    	printf("Error: Lookup does not return expected value for key T.\n");
    }

    if (strcmp(lookup(D1, "M"), "OceanView") != 0) {
    	printf("Error: Lookup does not return expected value for key T.\n");
    }
    if (strcmp(lookup(D1, "L"), "Taraval") != 0) {
    	printf("Error: Lookup does not return expected value for key T.\n");
    }
    if (strcmp(lookup(D1, "K"), "Ingleside") != 0) {
    	printf("Error: Lookup does not return expected value for key T.\n");
    }
    if (strcmp(lookup(D1, "J"), "Church") != 0) {
    	printf("Error: Lookup does not return expected value for key T.\n");
    }

    makeEmpty(D1);

    //add some more items to D1
    insert(D1, "5", "Fulton");
    insert(D1, "6", "Haight/Parnassus");
    insert(D1, "14", "Mission");

    stringD1 = DictionaryToString(D1);
    printf("\n%s", stringD1);
    free(stringD1);

    //make sure it's still the expected size
    if (size(D1) != 3) {
    	printf("Error: Dictionary not expected size.\n");
    } 
    
    freeDictionary(&D1);   
    return EXIT_SUCCESS;
}

