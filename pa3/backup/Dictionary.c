/* Rohan Tuli
CruzID: rtuli
Class: CSE 15
Date: 11/13/2019 (extended)
Description: Dictionary ADT
*/


#include "Dictionary.h"
#include <assert.h>
#include <string.h>

//defining the node object
typedef struct NodeObj{
   char* k; //key
   char* v; //value
   struct NodeObj* next;
} NodeObj;

typedef NodeObj* Node;

//defining the dictionary object
typedef struct DictionaryObj {
	Node head;
	int numItems;
} DictionaryObj;


//create a new node
Node newNode (char* value, char* key) {
	Node N = malloc(sizeof(NodeObj)); //allocate memory for the new node
	assert(N != NULL); //make sure the new node actually was created

    N->k = key;
    N->v = value;
/*
    N->k = calloc(strlen(key) + 1, sizeof(char)); //allocate memory for the key (including terminator)
	strcpy(N->k, key); //set the key 

    N->v = calloc(strlen(value) + 1, sizeof(char)); //allocate memory for the ACTUAL VALUES (including terminator)
	strcpy(N->v, value); //set the ACTUAL VALUES
*/
	N->next = NULL; //the new doesn't point to any nother node

	return N;
}

//create a new dictionary
Dictionary newDictionary() {
    //printf("NEW VERSION OF THE PROGRAM!\n");
	Dictionary D = malloc(sizeof(DictionaryObj));
	D->head = NULL;
	D->numItems = 0;
	return D;
}

//I wonder what freeDictionary does? 
void freeDictionary(Dictionary* pD) {
	makeEmpty(*pD); //delete all of the nodes currently in dictionary
	free(*pD);
	*pD = NULL;
}


void insert(Dictionary D, char* k, char* v) {
    //make sure a node with this key doesn't already exist
    if (lookup(D, k) != NULL) {
        return;
    }
    
    //create a new node
	Node N = newNode(v, k);

    
    
	N->next = NULL; //new node is at the end, so it doesn't point to anything
	//if the dictionary is empty, make the new node the head of the linked list
	if (D->numItems == 0) {
		D->head = N;
	} else {
		Node Temp = D->head; //create a temporary node that can traverse the list
		while (Temp->next != NULL) { //go until the last node
			Temp = Temp->next; //move forwards by one node
		}
        //now the temp node is the last node
		Temp->next = N; //make the last node (which is also the temp node) point to the new node
	}

	D->numItems++;
}

//yes
int size(Dictionary D) {
	return D->numItems;
}

//delete all the nodes in the dictionary except the head
void makeEmpty(Dictionary D) {
	Node N = NULL; //create a new temporary node
	while (D->numItems > 1) { //go until there are no more nodes
		N = D->head;
		D->head = D->head->next;
		N->next = NULL;
		free(N);
        N = NULL;
		D->numItems--;
	}
    
    //delete the head
    N = D->head;
    free(N);
    N = NULL;
    D->head = NULL;

	D->numItems = 0;
}


char* lookup(Dictionary D, char* k) {
    //check to make sure the list isn't empty
    if (D->numItems == 0) {
        return NULL;
    } 
    
	Node N = D->head;
    
	while (N != NULL) { 
		if (!strcmp(N->k, k)) {
			return N->v;
		}
		N = N->next;
	}

	return NULL;
}


void delete(Dictionary D, char* k) {
	//printf("Delete called\n");	
	Node N = D->head;

    //make sure the thing being deleted exists
    if (lookup(D, k) == NULL) {
        return;
    }

	//if there's only one note, delete only that node and don't relink
	if (D->numItems == 1) {
		D->head = NULL;
		D->numItems = 0;
        //free(N->k); //free the memory used by the key string
        //free(N->v); //free the memory used by the ACTUAL VALUES string
		free(N);
        N = NULL;
        return;
	}

    //for the first item of the list
    if (!strcmp(N->k, k)) {
        D->head = N->next;
        free(N);
        N = NULL;
        return;
    }

	//for multiple items
	while (N->next != NULL && strcmp((N->next)->k, k) != 0) {        
            N = N->next;
	}
    
	Node DeleteThis = N->next;
	N->next = DeleteThis->next;

    free(DeleteThis);
    DeleteThis = NULL;
    
    D->numItems--;    

}

// DictionaryToString()
// Determines a text representation of the current state of Dictionary D. Each 
// (key, value) pair is represented as the chars in key, followed by a space,
// followed by the chars in value, followed by a newline '\n' char. The pairs 
// occur in the same order they were inserted into D. The function returns a
// pointer to a char array, allocated from heap memory, containing the text  
// representation described above, followed by a terminating null '\0' char. 
// It is the responsibility of the calling function to free this memory.
char* DictionaryToString(Dictionary D) {
    //count how long all the entries are
    int contentLength = 0;

    Node N = D->head;

    //get the length of all the key and value strings
	while (N != NULL) {
        contentLength += strlen(N->k); //length of key without null terminator
        contentLength += strlen(N->v); //length of ACTUAL VALUES without null terminator
        contentLength += 2; //space between key and values, and newline character
		N = N->next;
	}

    contentLength++; //add a character for the null terminator

    //create a string to store the output
	char* output;
    //allocate memory for the output string, using the length of all stored strings plus a null terminator
    output = calloc(contentLength, sizeof(char));
    strcpy(output, ""); //make output an empty string

	Node M;
	for (M = (D->head); M != NULL; M = M->next) {
        //char* temp = calloc(strlen(M->k) + strlen(M->v) + 2, sizeof(char));
        strcat(output, M->k);
        strcat(output, " ");
        strcat(output, M->v);
        strcat(output, "\n");
        //sprintf(temp,"%s%s %s\n", output, M->k, M->v);
        //output = strcat(output, temp);
        //free(temp);
	}
    
    strcat(output, "\0");

	return output;
    
}
