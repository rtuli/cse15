/* Rohan Tuli
CruzID: rtuli
Class: CSE 15
Date: 12/03/2019 (extended)
Description: Integer Queue ADT
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "IntegerQueue.h"

//Nodes and stuff ------------------------------------------------------------
//defining the node 
typedef struct NodeObj {
    int integer;
    struct NodeObj* next;
} NodeObj;

typedef NodeObj* Node;

//create a new node
Node newNode (int inputInteger) {
    Node N = malloc(sizeof(NodeObj)); //allocate memory for the node
    assert(N != NULL); //make sure the node was actually created
    N->integer = inputInteger; //set the integer in the node to be the input integer
    //don't point to any other nodes
    N->next = NULL;
    return N;
}

// Exported type --------------------------------------------------------------

typedef struct IntegerQueueObj {
    Node head;
    Node tail;
    int numItems;
} IntegerQueueObj;


// newIntegerQueue()
// Constructor for the IntegerQueue ADT
IntegerQueue newIntegerQueue() {
    IntegerQueue Q = malloc(sizeof(IntegerQueueObj));
    //it's empty, so the head and tail don't point to anything, and there are no items
    Q->head = NULL;
    Q->tail = NULL;
    Q->numItems = 0;
    return Q;
}

// freeIntegerQueue()
// Destructor for the Queue ADT
void freeIntegerQueue(IntegerQueue* pQ) {
    dequeueAll(*pQ); //remove everything from the queue
    free(*pQ); //free the memory allocated to the queue
    *pQ = NULL; //set the queue to be null
}


// ADT operations -------------------------------------------------------------

// isEmpty()
// Returns 1 (true) if Queue Q is empty, 0 (false) otherwise.
int isEmpty(IntegerQueue Q) {
    if (Q->numItems == 0) {
        return 1;
    } else {
        return 0;
    }

}

// length()
// Returns the number of elements in Q
int length(IntegerQueue Q) {
    return Q->numItems;

}

// enqueue()
// Inserts x at back of Q.
void enqueue(IntegerQueue Q, int x) {
    //printf("nq()\n");
    //create a new Node
    Node N = newNode(x);
    
    //if the queue is empty
    if (Q->numItems == 0) {
        Q->head = N;
        Q->tail = N;
        Q->numItems++;
        return;
    }

    //if there is one item in the queue
    if (Q->numItems == 1) { //if the head and the tail node are the same nodes
        Q->tail = N; //tail is the node that was just inserted        
        Q->head->next = Q->tail; //node .next is the tail node
        Q->numItems++;
        return;
    }    
    
    Q->tail->next = N; //the last node .next is the new node
    Q->tail = N; //the new node is last now, so it becomes the tail
    Q->tail->next = NULL; //the tail points to nothing because it is back at the end

    //increment the item counter
    Q->numItems++;

}

// dequeue()
// Deletes and returns the item at front of Q.
// Pre: !isEmpty()
int dequeue(IntegerQueue Q) {
    //printf("dq()\n");
    //check if queue is empty
    if (Q->numItems == 0) {
        //print an error?
        //fprintf(stderr, "Error: Attempted to dequeue() from an empty queue\n");
        return 0;
    }

    //create an int to store the output, since that node will be deleted before we can return
    int output = Q->head->integer;
    
    //delete that node
    Node DeleteThis = Q->head;
    Q->head = DeleteThis->next; //head is now the node after the old head
    
    //free memory for the deleted node
    free(DeleteThis);
    DeleteThis = NULL;

    Q->numItems--;
    //printf("Dequeue: %d\n", output);
    return output;
    
}

// peek()
// Returns the item at front of Q.
// Pre: !isEmpty()
int peek(IntegerQueue Q) {
        //printf("peek()\n");
    //check if queue is empty
    if (Q->numItems == 0) {
        //print an error?
        //fprintf(stderr, "Error: Attempted to peek() from an empty queue\n");
        exit(1);
    }
    
    return Q->head->integer;
    
}

// dequeueAll()
// Resets Q to the empty state.
void dequeueAll(IntegerQueue Q) {
    //printf("dqa()\n");
    Node N = NULL; //temporary node
    while (Q->numItems > 1) {
        N = Q->head;
        Q->head = Q->head->next;
        N->next = NULL;
        free(N);
        N = NULL;
        Q->numItems--;
    }

    //delete the head
    N = Q->head;
    free(N);
    N = NULL;
    Q->head = NULL;
    Q->numItems = 0;
}

// Other Operations -----------------------------------------------------------

// IntegerQueueToString()
// Determines a text representation of IntegerQueue Q. Returns a pointer to a 
// char array, allocated from heap memory, containing the integer sequence 
// represented by Q. The sequence is traversed from front to back, each integer
// is added to the string followed by a single space. The array is terminated 
// by a NUL '\n' character. It is the responsibility of the calling function to 
// free this heap memory.
char* IntegerQueueToString(IntegerQueue Q) {
    //allocate memory for the output string based on how many integers there are in the queue
    int stringLength = Q->numItems * 2;
    stringLength++; //terminating char
    char* output = calloc(stringLength, sizeof(char));
    strcpy(output, "");

    Node N = Q->head;

    for (int i = 0; i < Q->numItems; i++) {
        char temp[8];
        sprintf(temp, "%d ", N->integer);
        strcat(output, temp);
        N = N->next;
    }

    strcat(output, "\0");
    return output;
}

// equals()
// Returns true (1) if Q and R are matching sequences of integers, false (0) 
// otherwise.
int equals(IntegerQueue Q, IntegerQueue R) {
    //printf("equals()\n");
    //if the two queues aren't the same length, they can't possibly be equal, so check for that first
    if (Q->numItems != R->numItems) {
        return 0;
    }

    int outputStatus = 1; //by default, assume that the two queues match

    Node N = Q->head;
    Node M = R->head;

    for (int i = 0; i < Q->numItems; i++) {
        if (N->integer != M->integer) {
            outputStatus = 0;
            //printf("N: %d\nM: %d\nOutput Status: %d\n", N->integer, M->integer, outputStatus);
        }
        //increment to the next set of nodes
        N = N->next;
        M = M->next;
    }
    
    return outputStatus;
}



