//Rohan Tuli PA4

#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include"Job.h"
#include"IntegerQueue.h"


// getjob()
// Reads two numbers from file in, and returns a new Job with those numbers
// as arrival and duration.
Job getJob(FILE* in){
   int a, d;
   fscanf(in, "%d %d\n", &a, &d);
   return newJob(a, d);
}

int main(int argc, char* argv[]) {
    int i; //use for loops and stuff
    
	//check for correct number of inputs----------------------------------------------------------
	if (argc != 2) {
	    exit(1);
	}

	// open files for reading and writing---------------------------------------------------------
	FILE *inputFile;
	FILE *reportFile;
	FILE *traceFile;

	inputFile = fopen(argv[1], "r");

	//report file
	char* reportFileName = calloc(strlen(argv[1]) + 4, sizeof(char));
	strcpy(reportFileName, argv[1]);
	strcat(reportFileName, ".rpt");
	reportFile = fopen(reportFileName, "w");
	free(reportFileName);

	//trace file
	char* traceFileName = calloc(strlen(argv[1]) + 4, sizeof(char));
	strcpy(traceFileName, argv[1]);
	strcat(traceFileName, ".trc");
	traceFile = fopen(traceFileName, "w");
	free(traceFileName);

	// read in m jobs from input file and place them in backup array---------------------------
	//get the number of jobs m
	int m = fgetc(inputFile) - 48; //get char from file, subtract 48 to make int
	fprintf(reportFile, "%d Jobs:\n", m); //output the number of lines/jobs to the report file

	//allocate an array of Job references of length m
	Job* backupArray = calloc(m, sizeof(Job));

	//get jobs from file
	for (i = 0; i < m; i++) {
		backupArray[i] = getJob(inputFile);
	}

	//print out the jobs to the report file
	for (i = 0; i < m; i++) {
		printJob(reportFile, backupArray[i]);
	}
    fprintf(reportFile, "\n\n"); //print a new line because printJob is stupid

    //print a nice line of stars in the report file
    for (i = 0; i < 25; i++) {
        fprintf(reportFile, "*");
    }
    fprintf(reportFile, "\n"); //new line

	// declare and initialize an array of m-1 processor Queues along with------------------------
	// any necessary storage queues
	IntegerQueue* processorQueueArray = calloc(m - 1, sizeof(IntegerQueue)); //array of processor queues
    IntegerQueue storageQueue = newIntegerQueue(); //single storage queue
	//
	// loop: run simulation with n processors for n=1 to n=m-1 {----------------------------------
    for (i = 1; i < m - 1; i++) {
	//    place Job indices from backup array into storage queue----------------------------------
        //if each job is assigned a number in order (1 through m),...
        //...then the queue stores these numbers and each number can...
        //...be used too lookup the corresponding job
        for (int j = 0; j < m; j++) {
            enqueue(storageQueue, j); //references the positon in the backup array
        }
        
        //create a timer to be used throughout this simulation
	//    loop: process all Jobs in this simulation { --------------------------------------------
        

	//
	//       determine the time of the next arrival or finish event and update time
	//
	//       if any Jobs finish now, then complete them in the order they appear in 
	//       the queue array, i.e. lowest index first.
	//
	//       if any Jobs arrive now, then assign them to a processor queue 
	//       of minimum length and with lowest index in the queue array.
	//  
         //} end loop----------------------------------------------------------------------------
	//    compute the total wait, maximum wait, and average wait for 
	//    all Jobs, then reset finish times for next simulation
	//
	} // } end loop -------------------------------------------------------------------------------
    
	//
	// free all heap memory, close input and output files------------------------------------------
    free(processorQueueArray);

	return EXIT_SUCCESS;
}
