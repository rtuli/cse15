//Rohan Tuli PA4

#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include"Job.h"
#include"IntegerQueue.h"


// getjob()
// Reads two numbers from file in, and returns a new Job with those numbers
// as arrival and duration.
Job getJob(FILE* in){
   int a, d;
   fscanf(in, "%d %d\n", &a, &d);
   return newJob(a, d);
}

int main(int argc, char* argv[]) {
    int i; //use for loops and stuff
    
	//check for correct number of inputs----------------------------------------------------------
	if (argc != 2) {
	    exit(1);
	}

	// open files for reading and writing---------------------------------------------------------
	FILE *inputFile;
	FILE *reportFile;
	FILE *traceFile;

	inputFile = fopen(argv[1], "r");

	//report file
	char* reportFileName = calloc(strlen(argv[1]) + 4, sizeof(char));
	strcpy(reportFileName, argv[1]);
	strcat(reportFileName, ".rpt");
	reportFile = fopen(reportFileName, "w");
	free(reportFileName);

	//trace file
	char* traceFileName = calloc(strlen(argv[1]) + 4, sizeof(char));
	strcpy(traceFileName, argv[1]);
	strcat(traceFileName, ".trc");
	traceFile = fopen(traceFileName, "w");
	free(traceFileName);

	// read in m jobs from input file and place them in backup array---------------------------
    int m;	
    //get the number of jobs m
    fscanf(inputFile, "%d", &m);
	//int m = fgetc(inputFile) - 48; //get char from file, subtract 48 to make int
	fprintf(reportFile, "%d Jobs:\n", m); //output the number of lines/jobs to the report file

	//allocate an array of Job references of length m
	Job* backupArray = calloc(m, sizeof(Job));

	//get jobs from file
	for (i = 0; i < m; i++) {
		backupArray[i] = getJob(inputFile);
	}

	//print out the jobs to the report file
	for (i = 0; i < m; i++) {
		printJob(reportFile, backupArray[i]);
	}
    fprintf(reportFile, "\n\n"); //print a new line because printJob is stupid

    //print a nice line of stars in the report file
    for (i = 0; i < 25; i++) {
        fprintf(reportFile, "*");
    }
    fprintf(reportFile, "\n"); //new line

	// declare and initialize an array of m-1 processor Queues along with------------------------
	// any necessary storage queues
	IntegerQueue* processorQueueArray = calloc(m - 1, sizeof(IntegerQueue)); //array of processor queues
    IntegerQueue storageQueue = newIntegerQueue(); //single storage queue

       

	//
	// loop: run simulation with n processors for n=1 to n=m-1 {----------------------------------
    //for (i = 1; i < m - 1; i++) {
	//    place Job indices from backup array into storage queue----------------------------------
        //if each job is assigned a number in order (1 through m),...
        //...then the queue stores these numbers and each number can...
        //...be used too lookup the corresponding job
        IntegerQueue storageQ = newIntegerQueue(); //create the Q
        for (int j = 0; j < m; j++) {
            enqueue(storageQ, j);
        }

         //initialize the timer to zero
        int mainTimer = 0;

        //create a timer to store the start time of the currently processed job
        int startTime;
    	int finishTime;

        //test print the queue 
        printf("storageQ: %s\n", IntegerQueueToString(storageQ));
        printf("pQ1: \n\n");

        //create one processor Q
        IntegerQueue pQ1 = newIntegerQueue(); //create the Q
        //the queue starts empty
        
        while (mainTimer < 20) {

            //calculate the finish time of the front item where finish time = start time + duration
            if (getFinish(backupArray[peek(pQ1)]) == UNDEF) { //if the item at the front of the processor

            }

            //check if it is time to stop processing the first item in the processor Q 
            if (!isEmpty(pQ1) && getFinish(backupArray[peek(pQ1)]) >= mainTimer) {
				enqueue(storageQ, dequeue(pQ1));
            }

            //check if it is time to start processing the first item in the storage Q
            if (getArrival(backupArray[peek(storageQ)]) == mainTimer) {
                //dequeue from the storage Q and enequeue to the processor Q            
                enqueue(pQ1, dequeue(storageQ));
            }

            //test print the queues 
            printf("storageQ: %s\n", IntegerQueueToString(storageQ));
            printf("pQ1: %s\n\n", IntegerQueueToString(pQ1));
            
        mainTimer++;
        }

	//} // } end loop -------------------------------------------------------------------------------
    
	//
	// free all heap memory, close input and output files------------------------------------------
    free(processorQueueArray);

	return EXIT_SUCCESS;
}
