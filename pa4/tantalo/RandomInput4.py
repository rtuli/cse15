#------------------------------------------------------------------------------
#  RandomInput4.py
#  Creates a random input file for CSE 15 pa4. Run this program by doing
#
#        python3 RandomInput4.py
#
#------------------------------------------------------------------------------
import random

# get user input and initialize local variables
filename = input("Enter name of input file to create: " )
num_jobs = int( input("Enter the number of jobs: ") )
max_arrival  = int( input("Enter the maximum arrival time: " ) )
max_duration = int( input("Enter the maximum duration: " ) )

# open file
f = open(filename, 'w')

# write first line
f.write(str(num_jobs)+"\n")

# generate random arrival times
A = [random.randrange(1, max_arrival) for i in range(num_jobs)]
A.sort()

#generate random durations
D = [random.randrange(1, max_duration) for i in range(num_jobs)]

# write remaining  lines
for i in range(num_jobs):
   f.write(str(A[i])+" "+str(D[i])+"\n")
# end for

# close file
f.close()
