/* Rohan Tuli
CruzID: rtuli
Class: CSE 15
Date: 10/28/2019 (extended)
Description: "This program uses recursion to find all solutions to the n-Queens problem, for 1 <= n <= 15" - The PA2 manual

*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_BOARD_SIZE 15

void placeQueen(int** B, int n, int i, int j) {
    B[i][j]++; //increment the square where the queen is placed
    B[i][0] = j; //at the beginning of each row, mark the column postion of the queen
    
    //k = row number
    //l = column number 
    int l;
    int k;

    //decrement all the squares in the same column below the queen
    for (k = i + 1; k <= n; k++) {
		B[k][j]--; //decrement each square in the column below	
    }

    //decrement all the squares in the negative diagonal
    k = i + 1;
    for (l = j + 1; l <= n && k <= n; l++) {    
        B[k][l]--;
        k++;
    }

    //decrement all the squares on the positive diagonal
    k = i + 1;
    for (l = j - 1; l >= 1 && k <= n; l--) {    
        B[k][l]--;
        k++;
    }    
    

}

void removeQueen(int** B, int n, int i, int j) {
    B[i][j]--; //decrement the square where the queen is placed
    B[i][0] = 0; //reset the column position marker at the beginning of each row

    //k = row number
    //l = column number 
    int l;
    int k;

    //increment all the squares in the same column below the queen
    for (k = i + 1; k <= n; k++) {
		B[k][j]++; //increment each square in the column below	
    }

    //increment all the squares in the negative diagonal
    k = i + 1;
    for (l = j + 1; l <= n && k <= n; l++) { //start to the bottom right of the queen, and go down and to the right
        B[k][l]++;
        k++; //go one row down
    }

    //increment all the squares on the positive diagonal
    k = i + 1;
    for (l = j - 1; l >= 1 && k <=n; l--) { //start to the bottom left of the queen, and go down and to the left until the left edge
        B[k][l]++;
        k++; //go one row down
    }    
    
}

void printBoard(int** B, int n) {
	printf("("); //print the opening parenthisis
    int i;    
    for (i = 1; i < n; i++) {
       printf("%d, ", B[i][0]); //print the array element
    }
    printf("%d)\n", B[i][0]); //print the last element and the closing parenthesis 
}



int findSolutions(int** B, int n, int i, char* mode) {
    int j; //col selector
    int sum = 0;
    if (i > n) { //if a queen was placed on row n, and hence a solution was found
        if (!strcmp(mode, "verbose")) { //if we are in verbose mode
            //print solution
            printBoard(B, n);
            /*
            int k;
            int l;
            //create an array to store the solution values before output
            int solutionArray[MAX_BOARD_SIZE];
            for (k = 1; k <= n; k++) {
                for (l = 1; l <= n; l++) {
                    if (B[k][l] == 1) {
                        solutionArray[k] = l; //set the position of the array that is the current row equal to the column that contains the queen
                    }
                }
            }
            
            //print from the solution array
            printf("(");   
            int m;
            for (m = 1; m < n; m++) {
                printf("%d, ", solutionArray[m]);
            }
            printf("%d)\n", solutionArray[m]);
            */
            
        }
        return 1;
    } else {
        for (j = 1; j <= n; j++) { //for each square on row i
            if (B[i][j] == 0) { //if that square is safe
                placeQueen(B, n, i, j); //place a queen on that square
                sum += findSolutions(B, n, i + 1, mode); //recur on row (i + 1), then add the returned value to an accumulating sum
                removeQueen(B, n, i, j); //remove the queen from that square
            }
        }
    }
    //printf("Sum: %d\n", sum);
    return sum;//return the accumulated sum
}


int main(int argc, char* argv[]) {
    //get input from command line arguments
	char* mode; //is verbose mode enabled?
    mode = (char*)calloc(7, sizeof(char)); //allocate memory for mode string
	int numQueens = 0; //number of queens

	if (argc == 2) {
		//check to make sure the second argument is a valid number
        if ((atoi(argv[1]) >= 1) & (atoi(argv[1]) <= MAX_BOARD_SIZE)) {
            numQueens = atoi(argv[1]);
        } else {
            fprintf(stderr, "%s\n", "Usage: Queens [-v] number\nOption: -v verbose output, print all solutions");
	        printf("%s\n", "Usage: Queens [-v] number\nOption: -v verbose output, print all solutions");
	        return 1; //return error
        } 
	} else if (argc == 3) {
		if (!strcmp(argv[1], "-v")) { //check to make sure the 2nd argument is -v
			strcpy(mode, "verbose"); //put the string "verbose" into mode
            //check to make sure the third argument is a valid number
            if ((atoi(argv[2]) >= 1) & (atoi(argv[2]) <= MAX_BOARD_SIZE)) {
                numQueens = atoi(argv[2]);
            } else {
                fprintf(stderr, "%s\n", "Usage: Queens [-v] number\nOption: -v verbose output, print all solutions");
		        printf("%s\n", "Usage: Queens [-v] number\nOption: -v verbose output, print all solutions");
		        return 1; //return error
            }
		} else {
            fprintf(stderr, "%s\n", "Usage: Queens [-v] number\nOption: -v verbose output, print all solutions");
		    printf("%s\n", "Usage: Queens [-v] number\nOption: -v verbose output, print all solutions");
		    return 1; //return error
		}
	} else {
        fprintf(stderr, "%s\n", "Usage: Queens [-v] number\nOption: -v verbose output, print all solutions");
		printf("%s\n", "Usage: Queens [-v] number\nOption: -v verbose output, print all solutions");
		return 1; //return error
	}

	//printf("Verbose: %s, Queens: %d\n", mode, numQueens);

    //create the board
    int** B;
    B = calloc(MAX_BOARD_SIZE + 1, sizeof(int*)); //allocate enough memory for an array of pointers
    for (int i = 0; i <= MAX_BOARD_SIZE; i++) {
        B[i] = calloc(MAX_BOARD_SIZE + 1, sizeof(int)); //allocate enough memory for an array of ints
    }
    //because calloc is used, the board is automatically populated with "0"s


    int numSolutions;
    numSolutions = findSolutions(B, numQueens, 1, mode);
    printf("%d Queens has %d solutions\n", numQueens, numSolutions);


    //printBoard(B, numQueens);

    free(mode);
    //free the memory used for the elements of each row array
    for (int m = 0; m < MAX_BOARD_SIZE + 1; m++) {
        free(B[m]);
    }
    free(B); //free the array of rows


    //now that the program has executed successfully, return 0
    return 0;
}
