#include<stdio.h>
#include<stdlib.h>

// Define fptr_t type
typedef int (*fptr_t)(int, int);

//functions
int sum(int x, int y) {
    return x + y;
}
 
int diff(int x, int y) {
    return x - y;
}

int prod(int x, int y) {
    return x * y;
}

int quot(int x, int y) {
    return x / y;
}

int rem(int x, int y) {
    return x % y;
}

int apply(fptr_t fp, int x, int y) {
    return fp(x, y);
}

int compute(fptr_t fcn[5], int* A, int* idx, int n) {
    int output = apply(fcn[idx[0]], A[0], A[1]);
    for (int i = 1; i < n; i++) {
        output = apply(fcn[idx[i]], output, A[i + 1]);
    }

    return output;
}

void testCompute(){
   fptr_t op[]  = {sum, diff, prod, quot, rem};
   int A[]      = {3, 2, 5, 4, 6, 7, 9, 2, 8};
   int opCode[] = {0, 2, 1, 4, 2, 2, 3, 1};
   int n = 8;

   //--------------------------------------------------------------------------
   // prints 86, which is the value of the value of the expression:
   //
   //               (((((((3+2)*5)-4)%6)*7)*9)/2)-8
   //
   //--------------------------------------------------------------------------
   printf("%d\n", compute(op, A, opCode, n)); 
}

int main(){

   testCompute();  // output should be 86

   return EXIT_SUCCESS;
}
