/* Rohan Tuli
CruzID: rtuli
Class: CSE 15L
Date: 11/22/2019 (Extension)
Description: This program performs arethmetic operations based on input from file.
*/

#include<stdio.h>
#include<stdlib.h>

// Define fptr_t type
typedef int (*fptr_t)(int, int);

//functions
int sum(int x, int y) {
    return x + y;
}
 
int diff(int x, int y) {
    return x - y;
}

int prod(int x, int y) {
    return x * y;
}

int quot(int x, int y) {
    return x / y;
}

int rem(int x, int y) {
    return x % y;
}

int apply(fptr_t fp, int x, int y) {
    return fp(x, y);
}

int compute(fptr_t fcn[5], int* A, int* idx, int n) {
    int output = apply(fcn[idx[0]], A[0], A[1]);
    for (int i = 1; i < n; i++) {
        output = apply(fcn[idx[i]], output, A[i + 1]);
    }

    return output;
}

int main(int argc, char* argv[]) {
	//make sure there is actually an argument
	if (argc != 2) {
		exit(1);
	}
    
    //create the array of function pointers
    fptr_t op[]  = {sum, diff, prod, quot, rem};

    FILE *inputFile;
    inputFile = fopen(argv[1], "r"); //open the input file in read mode
    
    //get the first line of the file (n)----------------------
    char* firstLine = calloc(100, sizeof(char));
    fgets(firstLine, 100, inputFile);
    int n = atoi(firstLine);
    free(firstLine);
    
    //print the number 
    //printf("Number: %d\n", n);

    //get the opcodes-----------------------------------------
    char* opcodeString = calloc(2 * n, sizeof(char));
    fgets(opcodeString, 2 * n , inputFile);
    //printf("Opcode String: %s\n", opcodeString);

    //put the opcodes into an array
    int* opcodes = calloc(n, sizeof(int));
    for (int i = 0; i < n; i++) {
        opcodes[i] = opcodeString[i * 2] - 48;
    }
    free(opcodeString);

    /*//print the opcode array
    printf("Opcode array: ");
    for (int i = 0; i < n; i++) {   
        printf("%d ", opcodes[i]);
    }
    printf("\n");*/
    
    //get the operands ---------------------------------------
    //the last line doesn't have a new line char at the end so fgets will not work
    //create am array to put the operands    
    int* operands = calloc(n + 1, sizeof(int));
    
    fgetc(inputFile); //skip over the newline character
    for (int i = 0; i <= 2 * (n + 1); i++) {
        char c = fgetc(inputFile);
        //check to make sure its a valid number
        if (c >= 49 && c <= 57) {
            operands[i / 2] = c - 48; //subtract 48 to make it an int
        } 
    }
    
    /*//print the operand array
    printf("Operand array: ");
    for (int i = 0; i < n + 1; i++) {   
        printf("%d ", operands[i]);
    }
    printf("\n");
    */
    
    fprintf(stdout, "%d", compute(op, operands, opcodes, n));

    //free the opcode and operand arrays
    free(opcodes);
    free(operands);
    
}
