/* Rohan Tuli
CruzID: rtuli
Class: CSE 15L
Date: 10/21/2019
Description: This program reads from a file, counts the number of each character type on each line, and outputs the results to another file
*/

//include standard libraries
#include <stdio.h>
#include <stdlib.h>

#define MAX_LINE_SIZE 100

void extract_chars(char* s, char* a, char* d, char* p, char* w) {
    for (int i = 0; i < MAX_LINE_SIZE; i++) {
        //check for an alphabetic character        
        if (((*s >= 0x42) & (*s <= 0x5A)) | ((*s >= 0x61) & (*s <= 0x7A))) {
            sprintf(a, "%s%c", a, *s); //add this character to the a string
        }
        //check for a decimal character        
        if ((*s >= 0x30) & (*s <= 0x39)) {
            sprintf(d, "%s%c", d, *s); //add this character to the a string
        }
        //check for a punctuation character        
        if (((*s >= 0x21) & (*s <= 0x2F)) | ((*s >= 0x3A) & (*s <= 0x40))| ((*s >= 0x5B) & (*s <= 0x60))|((*s >= 0x7B) & (*s <= 0x7E))) {
            sprintf(p, "%s%c", p, *s); //add this character to the a string
        }
        //check for a whitespace character
        if (*s == 0x20) {
            sprintf(w, "%s%c", w, *s); //add this character to the a string
        }
        
        //increment the string pointer
        s++;
    } 
}

//custom function to get the string length
int string_length(char* string) {
    int stringLength = 0;    
    while (*string != 0x0) { //while not the null termination character
        stringLength++;
        string++;
    }
    return stringLength;
}

int main(int argc, char* argv[]) {
    //make sure the correct number of command line arguments have been input
    if (argc != 3) {
        printf ("Bad\n");
        return 1;
    
    }

    FILE *inputFile;
    FILE *outputFile;
    
    inputFile = fopen(argv[1], "r"); //open the input file in read mode
    outputFile = fopen(argv[2], "w"); //open the output file in write mode  

    int lineCounter = 1; //counter for the currently read line mumber (starts at line 1)
    //char *nextLine = calloc(MAX_LINE_SIZE + 1, sizeof(char)); //variable for if the next line is present, becomes NULL when no new lines
   
    
    char* lineString;
    lineString = calloc(MAX_LINE_SIZE + 1, sizeof(char));


    while (fgets(lineString, MAX_LINE_SIZE, inputFile)) { //while this isn't the last line 
        //nextLine = fgets(lineString, MAX_LINE_SIZE, inputFile); //read the current line
        //printf("nextLine: %c\n", nextLine);
        //create strings to hold the different types of characters 
        char* a; //alphabetic
        char* d; //decimal
        char* p; //punctuation
        char* w; //whitespace

        //allocate strings a, d, p, w on the heap
        a = calloc(MAX_LINE_SIZE + 1, sizeof(char));
        d = calloc(MAX_LINE_SIZE + 1, sizeof(char));
        p = calloc(MAX_LINE_SIZE + 1, sizeof(char));
        w = calloc(MAX_LINE_SIZE + 1, sizeof(char));
        
        
        //extract the characters for each line
        extract_chars(lineString, a, d, p, w);
        

        //send the necissary data to a string that is put in the output file
        fprintf(outputFile, "line %d contains:\n%d alphabetic characters: %s\n%d numeric characters: %s\n%d punctuation characters: %s\n%d whitespace characters: %s\n\n", lineCounter, string_length(a), a, string_length(d), d, string_length(p), p, string_length(w), w);     

        lineCounter++;

        //free all the allocated memory before starting again
        free(a);
        free(d);
        free(p);
        free(w);

    }

    fclose(outputFile); //close the output file

    //free the memory that hasn't yet been freed
    //free(nextLine);
    free(lineString);
    
}
