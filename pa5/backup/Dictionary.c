/*
Rohan Tuli
CruzID: rtuli
Class: CSE15
Date: 12/6/2019
Description: Dictionary ADT based on a the hash table datastructure.
*/

#include <string.h>
#include <assert.h>

#include "Dictionary.h"

// Provided funtions from HashDemo.c ///////////////////////////////////////////////////////////////////////////////////////

const int tableSize = 101;

// rotate_left()
// rotate the bits in an unsigned int
unsigned int rotate_left(unsigned int value, int shift) {
   int sizeInBits = 8*sizeof(unsigned int);
   shift = shift & (sizeInBits - 1);   // same as: shift = shift%sizeInBits
   if ( shift == 0 )
      return value;
   else
      return (value << shift) | (value >> (sizeInBits - shift)); // perform rotation
}

// pre_hash()
// turn a string into an unsigned int
unsigned int pre_hash(char* input) {  // input points to first char in string
   unsigned int result = 0xBAE86554;  // = 10111010111010000110010101010100, begin with a random looking bit pattern
   while (*input) {                   // while *input is not '\0' (not end of string)
      result ^= *input++;                // result = result ^ *input (current char alters result) 
                                         // input++  (go to next char)
      result = rotate_left(result, 5);   // rotate result by fixed amount
   }
                   // after the preceding loop, each char of input has altered result, which
   return result;  // is now a random looking bit pattern depending on the input string
}

// hash()
// turns a string into an int in the range 0 to tableSize-1
int hash(char* key){
   return pre_hash(key)%tableSize;
}

//Nodes and stuff /////////////////////////////////////////////////////////////////////////////////////////////////////////

//defining the node struct
typedef struct NodeObj{
   char* k; //key
   char* v; //value
   struct NodeObj* next;
} NodeObj;

typedef NodeObj* Node;

//create a new node
Node newNode(char* key, char* value) {
	Node N = malloc(sizeof(NodeObj)); //allocate memory for the new node
	assert(N != NULL); //make sure the new node actually was created

    N->k = key;
    N->v = value;

	N->next = NULL; //the new doesn't point to any nother node

	return N;
}


// Functions from Dictionary.h ///////////////////////////////////////////////////////////////////////////////////////////
// Dictionary
typedef struct DictionaryObj {
    Node* nodeArray;
    int numItems;
} DictionaryObj;


// Constructors-Destructors ---------------------------------------------------

// newDictionary()
// Constructor for the Dictionary ADT.
Dictionary newDictionary() {
    Dictionary D = malloc(sizeof(DictionaryObj)); //allocate memory for the dictionary
    D->nodeArray = calloc(tableSize, sizeof(NodeObj));//allocate memory for the array
    D->numItems = 0; //there are no items in the dictionary
    return D;
}

// freeDictionary()
// Destructor for the Dictionary ADT.
void freeDictionary(Dictionary* pD) {
    makeEmpty(*pD); //delete all of the nodes currently in the dictionary
    free((*pD)->nodeArray); //free the array
    free(*pD); //free the dictionary
    *pD = NULL;
}


// ADT operations -------------------------------------------------------------

// size()
// Return the number of (key, value) pairs in Dictionary D.
int size(Dictionary D) {
    return D->numItems;
}

// lookup()
// If D contains a pair whose key matches argument k, then return the 
// corresponding value, otherwise return NULL.
char* lookup(Dictionary D, char* k) {
    //make sure the list isn't emptyclear
    if (D->numItems == 0) {
        return NULL;
    }
    
    //if there's nothing in that slot of the array, then that key value is not stored
    if (D->nodeArray[hash(k)] == NULL) { 
        return NULL;
    }
    
    //assuming that that slot exists, traverse through the linked list until the key is found
    Node FindMe;
    for (FindMe = D->nodeArray[hash(k)]; FindMe->next == NULL; FindMe = FindMe->next) {
        if (strcmp(FindMe->k, k) == 0) { //if the key in the list matches the key being searched for
            return FindMe->v; //return the value corresponding to the key
        } 
    }
    
    //everything is completely broken and the function somehow gets this far
    return NULL;
}

// insert()
// Insert the pair (k,v) into D.
// Pre: lookup(D, k)==NULL (D does not contain a pair whose first member is k.)
void insert(Dictionary D, char* k, char* v) {
    //add a node to the linked list at that slot in the array
    Node InsertThis = newNode(k, v); //create the node
    if (D->nodeArray[hash(k)] == NULL) { //if that slot is empty
        D->nodeArray[hash(k)] = InsertThis; //put the node in the array
    } else { //if it's not empty 
        Node TempNode = D->nodeArray[hash(k)]; //go to first node
        //traverse the list
        while (TempNode->next != NULL) {
            TempNode = TempNode->next;
        }
        //add node N to the end of the list
        TempNode->next = InsertThis;
    }

    D->numItems++;
}

// delete()
// Remove pair whose first member is the argument k from D.
// Pre: lookup(D,k)!=NULL (D contains a pair whose first member is k.)
void delete(Dictionary D, char* k) {
    //make sure the thing to be deleted actually exists
    if (lookup(D, k) == NULL) {
        return;
    }

    Node TempNode = D->nodeArray[hash(k)]; //node to be deleted

    //if there's only one node in the slot, delete the one node
    if (TempNode->next == NULL && TempNode->k == k) {
		D->nodeArray[hash(k)] = NULL;
		D->numItems--;
		free(TempNode);
    	TempNode = NULL;
    	return;
    }


    //go through the list at the slot until the node before the one to be deleted
    while (TempNode->next != NULL && strcmp((TempNode->next)->k, k) != 0) {
    	TempNode = TempNode->next;
    }

    //stitch
	Node DeleteMe = TempNode->next;
	TempNode->next = DeleteMe->next;

	//memory stuff
	free(DeleteMe);
	DeleteMe = NULL;

	D->numItems--;

}

// makeEmpty()
// Reset D to the empty state, the empty set of pairs.
void makeEmpty(Dictionary D) {
	//check to see if it's already empty. No point in emptying an already empty dictionary, right?
	if (D->numItems == 0) {
		return;
	}

	//go through each possible slot in the array
	for (int i = 0; i < tableSize; i++) {
		if (D->nodeArray[i] != NULL) { //if there actually is something in that slot
			Node DeleteMe = NULL;
			Node HeadNode = D->nodeArray[i];
			while (HeadNode != NULL) {
				DeleteMe = HeadNode;
				HeadNode = HeadNode->next;
				DeleteMe->next = NULL;
				free(DeleteMe);
				DeleteMe = NULL;
				D->numItems--;
			}
		}
	}

}


// Other Operations -----------------------------------------------------------

// DictionaryToString()
// Determines a text representation of the current state of Dictionary D. Each 
// (key, value) pair is represented as the chars in key, followed by a space,
// followed by the chars in value, followed by a newline '\n' char. The pairs 
// occur in alphabetical order by key. The function returns a pointer to a char 
// array, allocated from heap memory, containing the text representation 
// described above, followed by a terminating null '\0' char. It is the 
// responsibility of the calling function to free this memory.
char* DictionaryToString(Dictionary D) {
	//count how long all the entries are
	int contentLength = 0;

	int i; //use this for for loops

	//go through each possible slot in the array
	for (i = 0; i < tableSize; i++) {
		if (D->nodeArray[i] != NULL) { //if there actually is something in that slot
			Node CountMe = D->nodeArray[i];
			while (CountMe != NULL) {
				contentLength += strlen(CountMe->k); //length of key
				contentLength += strlen(CountMe->v); //length of value
				contentLength += 2; //length of space between key and value + length of newline character
				CountMe = CountMe->next; //go to next node
			}
		}

		//printf("Count: %d\n", contentLength);
	}

	//create a string to store the output
	char* output = calloc(contentLength + 1, sizeof(char)); //heap memory including length of null terminating character

	//go through each possible slot in the array
	for (i = 0; i < tableSize; i++) {
		if (D->nodeArray[i] != NULL) { //if there actually is something in that slot
			Node TempNode = D->nodeArray[i];
			while (TempNode != NULL) {
				strcat(output, TempNode->k);
		        strcat(output, " ");
		        strcat(output, TempNode->v);
		        strcat(output, "\n");
				TempNode = TempNode->next; //go to next node
			}
		}
	}

	strcat(output, "\0");

	return output;
	
}
