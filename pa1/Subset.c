/* Rohan Tuli
CruzID: rtuli
Class: CSE 15
Date: 10/12/2019
Description: This program prints subsets of k length from a n length set.
*/

#include <stdio.h>
#include <stdlib.h>

//define a maximum value for n
#define MAX_SIZE 100

void printSet(int B[], int n) {
    //creat an empty string that will be used for the output
    char output[MAX_SIZE * 4]; //maximum  number of set elements * (2 digits per element (usually) + comma + space) 

    int setItems = 0; //how many items are in the set

    for (int i = 1; i <= n; i++) {
        if ((B[i] == 1) & (i == n)) {
            sprintf(output, "%s%d", output, i); //add the set element without any extra characters at the end to the output buffer
            setItems++;
        } else if (B[i] == 1) {
            sprintf(output, "%s%d, ", output, i); //add the set element followed by a comma and a space to the end of the output buffer
            setItems++;
        }
    }

    //determine if the string is empty
    if (setItems == 0) {
        sprintf(output, " "); //add a space to the output buffer
    }

    //print the set
    printf("{%s}\n", output);
}


void printSubsets(int B[], int n, int k, int i) {
	B[i] = 1;
	printSubsets(B, n, k - 1, i + 1);

	if (k == 0) {
		printSet(B, n);
		return;
	}

	B[i] = 0;
	printSubsets(B, n, k, i + 1);


}


// NEED TO MAKE SURE IT DOESN'T ACCEPT LETTERS AS INPUTS
int main(int argc, char *argv[]) {
    //create placeholder variables for n and k
    int n = 0;
    int k = 0;

    if (argc != 3) {
        //print the instructions if the program arguments are invalid
        printf("Usage: Subset n k (n and k are ints satisfying 0<=k<=n<=100)\n");
        return 1;
    } else {
        //set n equal to the second element of argv, and k equal to the third argument (1st element is argc)
        n = atoi(argv[1]);
        k = atoi(argv[2]);

        //check that n and k are not invalid
        if (!((k <= n) & (0 <= k) & (n <=MAX_SIZE))) {
            printf("Usage: Subset n k (n and k are ints satisfying 0<=k<=n<=100)\n");
            return 1;
        }
    }

//    printf("done"); //for testing purposes

    //create an array full of zeros for the length of the input set
    int B[n + 1];
    for (int i = 1; i <= n; i++) {
        B[i] = 0;
    }

    //printSubsets being passed: the array of zeros, the length of the set, the desired subset length, the start element in the set
    printSubsets(B, n, k, 1);


/*
    //create an empty array of size n + 1
    int B[n + 1];

    //fill the array
    B[0] = 0; //B[0] is unimpoirtant
    for (int i = 1; i <= n; i++) {
        
    }

*/
}

