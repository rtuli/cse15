//-----------------------------------------------------------------------------
// ParseFileIO.c
// Reads each word in an input file, parses it as int, double or string, then
// prints each word with its classification to the output file.
//-----------------------------------------------------------------------------
#include<stdio.h>
#include<stdlib.h>

int main(int argc, char * argv[]){
   FILE *in;         // input file pointer
   FILE *out;        // output file pointer 
   char word[200];   // stores one string from the input file 
   int status, n;
   double x;
   char ch;

   // Check command line for correct number of arguments.
   if( argc != 3 ){
      printf("Usage: %s <input file> <output file>\n", argv[0]);
      exit(EXIT_FAILURE);
   }

   // Open input file for reading. 
   in = fopen(argv[1], "r");
   if( in==NULL ){
      printf("Unable to read from file %s\n", argv[1]);
      exit(EXIT_FAILURE);
   }

   // Open output file for writing. 
   out = fopen(argv[2], "w");
   if( out==NULL ){
      printf("Unable to write to file %s\n", argv[2]);
      exit(EXIT_FAILURE);
   }

   // Read words from the input file and parse them as int, double or string.
   while(fscanf(in, " %s", word)!=EOF){ 
      // the space in the format string says to skip any number of leading
      // white space characters. We end when EOF (end-of-file) is reached. 
      // This operation will fail if the string has more than 200 characters, 
      // since that is the length of array word[].
      
      // try to parse word as int
      status = sscanf(word, "%d%c", &n, &ch);  // why is ch needed?
      if(status==1){
         fprintf(out, "%s is an int\n", word);
         continue; // skip rest of this iteration
      }

      // try to parse word as double
      status = sscanf(word, "%lf", &x);
      if(status==1){
         fprintf(out, "%s is a double\n", word);
         continue; // skip rest of this iteration
      }

      // word must be a non-numeric string
      fprintf(out, "%s is a non-numeric string\n", word);
 
   }

   // close input and output files 
   fclose(in);
   fclose(out);

   return(EXIT_SUCCESS);
}
