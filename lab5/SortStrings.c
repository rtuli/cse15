/* Rohan Tuli
CruzID: rtuli
Class: CSE 15L
Date: 12/01/2019
Description: This program reads strings from a file, puts them in an array, and outputs 
    the sorted file to a different array. The input and output files are specified in the 
    command line arguments.
*/
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>

#define MAX_STRING_LENGTH 20
/*
void swap(int* A, int i, int j){
   int temp;
   temp = A[i];
   A[i] = A[j];
   A[j] = temp;
}
*/

void swap(char** A, int i, int j) {
	char temp[MAX_STRING_LENGTH];
	strcpy(temp, A[i]);
	strcpy(A[i], A[j]);
	strcpy(A[j], temp);
}

/*
int Partition(int* A, int p, int r){
   int i, j, x;
   x = A[r];
   i = p-1;
   for(j=p; j<r; j++){
      if( A[j]<=x ){
         i++;
         swap(A, i, j);
      }
   }
   swap(A, i+1, r);
   return(i+1);
}
*/

int Partition(char** A, int p, int r) {
	int i, j;
	char temp[MAX_STRING_LENGTH];
	strcpy(temp, A[r]);
	i = p - 1;
	for (j = p; j < r; j++) {
		if (strcmp(A[j], temp) <= 0) {
			i++;
			swap(A, i, j);
		}
	}
	swap(A, i + 1, j);
	return (i + 1);

}

/*
void QuickSort(int* A, int p, int r){
   int q;
   if( p<r ){
      q = Partition(A, p, r);
      QuickSort(A, p, q-1);
      QuickSort(A, q+1, r);
   }
}
*/

void QuickSort(char** A, int p, int r) {
	int q;
	if (p < r) {
		q = Partition(A, p, r);
		QuickSort(A, p, q - 1);
		QuickSort(A, q + 1, r);
	}
	
}

void printArray(FILE* out, char** A, int n) {
    for (int i = 0; i < n; i++) {
        fprintf(out, "%s", A[i]);
    }
}

int main(int argc, char* argv[]) {
    //check for valid number of inputs
    if (argc != 3) {
        printf("Usage: %s <input file> <output file>\n", argv[0]);
		exit(EXIT_FAILURE);
    }

    //open input file, read select
    FILE *inputFile;
	inputFile = fopen(argv[1], "r");
	if (inputFile == NULL) {
		printf("Unable to read from file %s\n", argv[1]);
		exit(EXIT_FAILURE);
	}
    
    //open the output file, write select
    FILE *outputFile;
    outputFile = fopen(argv[2], "w");
    if (outputFile == NULL) {
		printf("Unable to write to file %s\n", argv[2]);
		exit(EXIT_FAILURE);
	}

    int numStrings = 0;
    //determine the number of strings
    fscanf(inputFile, "%d", &numStrings);
    //numStrings = fgetc(inputFile) - 48; //get char from file, subtract 48 to make int
    
    //skip the newline character
    fgetc(inputFile);

    //create an array to put all the strings
    char** stringArray;
    stringArray = calloc(numStrings, sizeof(char*)); 
    for (int i = 0; i < numStrings; i++) {
        //read the string from the file and put in a temp string (on the stack)
        char tempString[MAX_STRING_LENGTH];
        fgets(tempString, MAX_STRING_LENGTH, inputFile);
        
        //take the temp string on the stack and put it in the stringArray on the heap
        stringArray[i] = calloc(strlen(tempString), sizeof(char));
        strcpy(stringArray[i], tempString);
    }
    
    //sort
    QuickSort(stringArray, 0, numStrings - 1);


    //print the array to file
	printArray(outputFile, stringArray, numStrings);
  

    //free the strings
    for (int i = 0; i < numStrings; i++) {
        free(stringArray[i]);
    }
    //free the string array
    free(stringArray);
    
    //close the input and output files
    fclose(inputFile);
    fclose(outputFile);
    
    return(EXIT_SUCCESS);
}
