/*Rohan Tuli
*/

#include <stdlib.h>
#include <stdio.h>
#include "IntegerStack.h"

//InitialSize
static const int InitialSize = 1;

//IntegerStackObj
struct IntegerStackObj {
   int* items;         // array of items
   int numItems;      // number of items 
   int physicalSize;  // current length of underlying array
} IntegerStackObj;

// newIntegerStack()
// Constructor for the IntegerStack ADT
IntegerStack newIntegerStack() {
    IntegerStack S = malloc(sizeof(IntegerStackObj)); //allocate memory for a new integer stack object
    S->items = calloc(InitialSize, sizeof(int)); //make the array 
    S->numItems = 0; // count of items begins at zero
    S->physicalSize = InitialSize; // = 1

    return S; //return the created IntegeterStack Object
}

// freeIntegerStack()
// Destructor for the Stack ADT
void freeIntegerStack(IntegerStack* pS) {
   if( pS != NULL && *pS != NULL ) {
      free((*pS)->items); 
      free(*pS);
      *pS = NULL;
   }
}

// isEmpty()
// Returns true (1) if S is empty, false (0) otherwise.
int isEmpty(IntegerStack S) {
    if( S==NULL ){
      fprintf(stderr, "IntegerStack Error: isEmpty() called on NULL IntegerStack reference");
      exit(EXIT_FAILURE);
   }

   return (S->numItems == 0); //if the number of items is zero, returns true, else returns false
        
}

//function to double array, taken from IntegerQueue 
void doubleItemArray(IntegerStack L){
   int i;
   int* newArray;
   int* oldArray = L->items;
   L->physicalSize *= 2;
   newArray = calloc(L->physicalSize, sizeof(int));
   for( i=0; i<(L->numItems); i++){
      newArray[i] = oldArray[i];
   }
   L->items = newArray;
   free(oldArray);
}

// push()
// Pushes x on top of S.
void push(IntegerStack S, int x) {
   // increase physical size of array if necessary
   if( (S->numItems)==(S->physicalSize) ){
      doubleItemArray(S);
      S->numItems--;
   }

   //printf("numItems: %d\n", S->numItems);
   //printf("physicalSize: %d\n", S->physicalSize);
   //printf("Items: %d\n", S->items[(S->numItems)]);

   //S->numItems = ((S->numItems)+1)%(S->physicalSize);

   //increase the item counter
   S->numItems++;

   //insert the element at the top
   S->items[S->numItems] = x;
}

// pop()
// Deletes and returns integer at top of S.
// Pre: !isEmpty(S)
int pop(IntegerStack S) {
    if (S == NULL) { //if the stack is empty
        fprintf(stderr, "IntegerStack Error: pop() called with empty stack");
        exit(EXIT_FAILURE);
    }
    int x = S->items[S->physicalSize - S->numItems];
    S->numItems--; //decrement the number of items in the stack
    //printf("X: %d\n", x);
    return x;
    
}

// peek()
// Returns integer at top of S.
// Pre: !isEmpty(S)
int peek(IntegerStack S) {
    if (S == NULL) { //if the stack is null
        fprintf(stderr, "IntegerStack Error: peek() called on NULL");
        exit(EXIT_FAILURE);
    }

    if (S->numItems == 0) { //if the stack is empty
        fprintf(stderr, "IntegerStack Error: peek() called on empty stack");
        exit(EXIT_FAILURE);
    }


    //return integer at top of stack
    return S->items[S->numItems];

}

// popAll()
// Resets S to the empty state.
void popAll(IntegerStack S) {
    if (S == NULL) { //if the stack is null
        fprintf(stderr, "IntegerStack Error: peek() called on NULL");
        exit(EXIT_FAILURE);
    }

    S->numItems = 0;
    //S->physicalSize = 0;

}


// printIntegerStack()
// Prints a space separated list of integers in S, from top to bottom, to the
// filestream out.
void printIntegerStack(FILE* out, IntegerStack S) {
	int i, n = S->numItems, m = S->physicalSize;
	   if( S==NULL ){
	      fprintf(stderr, "IntegerQueue Error: printIntegerQueue() called on NULL "\
	                      "IntegerQueue reference");
	      exit(EXIT_FAILURE);
	   }

	   for(i=0; i<n; i++){
	      fprintf(out, "%d ", S->items[((S->numItems)-i)%m] );
	   }
   	fprintf(out, "\n");
}

// equals()
// Returns true (1) if S and T are matching sequences of integers, false (0) 
// otherwise.
int equals(IntegerStack S, IntegerStack T) {
   int i, n, m, eq=0;
   if( S==NULL || T==NULL ){
      fprintf(stderr, "IntegerQueue Error: equals() called on NULL IntegerQueue "\
                      "reference");
      exit(EXIT_FAILURE);
   }

   eq = ( (S->numItems)==(T->numItems) );
   for(i=0; eq && i<(S->numItems); i++){
      n = ((S->numItems)+i)%(S->physicalSize);
      m = ((T->numItems)+i)%(T->physicalSize);
      eq = ( (S->items[n])==(T->items[m]) );
   }
   return eq;
}
