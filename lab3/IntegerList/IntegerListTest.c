//-----------------------------------------------------------------------------
// IntegerListTest.c
// Test client for the IntegerList ADT
//-----------------------------------------------------------------------------
#include<stdio.h>
#include<stdlib.h>
#include"IntegerList.h" 

int main(){

   IntegerList A = newIntegerList();
   IntegerList B = newIntegerList();

   printf("%s\n", isEmpty(A)?"true":"false");
   printf("%d\n", size(A));

   add(A, 1, 2);
   add(A, 2, 3);
   add(A, 1, 1);

   printf("%s\n", isEmpty(A)?"true":"false");
   printf("%d\n", size(A)); 
   printIntegerList(stdout, A); 

   add(B, 1, 6);
   add(B, 1, 5);
   add(B, 1, 4);
 
   printIntegerList(stdout, B);

   add(A, 4, 4);
   add(A, 5, 5);
   add(A, 6, 6);
   
   printIntegerList(stdout, A);

   delete(A, 3);
   delete(A, 2);
   printf("%s\n", equals(A, B)?"true":"false");
   delete(A, 1);
   printf("%s\n", equals(A, B)?"true":"false");

   deleteAll(A);

   printf("%s\n", isEmpty(A)?"true":"false");
   printf("%d\n", size(A));  
   printIntegerList(stdout, A);

   freeIntegerList(&A);
   freeIntegerList(&B);

   return(EXIT_SUCCESS);
}

