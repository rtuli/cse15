/* Rohan Tuli
CruzID: rtuli
Class: CSE 15L
Date: 10/2/2019
Description: This program prints out the text "Route 20D, UCSC - Delaware". It is a hello world program.
*/

#include <stdio.h>
#include <stdlib.h>
#define OUTPUT_STRING "Route 20D, UCSC - Delaware\n"

int main() {
    printf("%s", OUTPUT_STRING);
    return EXIT_SUCCESS;
}
